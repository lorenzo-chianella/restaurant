package it.pegaso.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.pegaso.restaurant.models.Cliente;

public interface ClienteRepo extends JpaRepository<Cliente, Long>{

}
