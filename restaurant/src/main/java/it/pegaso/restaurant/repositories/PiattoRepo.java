package it.pegaso.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.pegaso.restaurant.models.Piatto;

public interface PiattoRepo extends JpaRepository<Piatto, Long>{

}
