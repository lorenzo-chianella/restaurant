package it.pegaso.restaurant.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import it.pegaso.restaurant.models.Ingrediente;

public interface IngredienteRepo extends JpaRepository<Ingrediente, Long>{

}
