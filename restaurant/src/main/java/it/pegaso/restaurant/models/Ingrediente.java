package it.pegaso.restaurant.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@EqualsAndHashCode
public class Ingrediente {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	private boolean isVegetariano;
	
	@ManyToMany(mappedBy = "ingredienti", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Piatto> piatto;
	
	@ManyToMany(mappedBy = "ingredienti", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Cliente> clienti;
}
